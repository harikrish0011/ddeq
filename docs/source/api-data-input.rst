Data input
==========
The data input component provides functions for reading remote sensing images,
hotspot locations and (effective) wind fields.

Source databases
----------------
The location and type of emission sources are used input for plume detection and
emission quantification. *ddeq* uses xarray datasets to store point source
information. The dataset contain source names (`source`), longitudes (`lon_o`),
latitudes (`lat_o`), labels for visualization (`label`) and source types (`type`).

CSV files
^^^^^^^^^
*ddeq* includes a small list of sources as a comma-separated values (CSV) file
that primarily contains cities and power plants used in previous studies.
User-defined files containing other sources can be prepared in the same format.

The CSV file can be read with the following function:

.. autofunction:: ddeq.misc.read_point_sources

CoCO2 point source database
^^^^^^^^^^^^^^^^^^^^^^^^^^^
*ddeq* includes the CoCO2 global emission point source database:

.. autofunction:: ddeq.coco2.read_ps_catalogue


Remote sensing images
---------------------
*ddeq* requires that trace gas images are provided as `xr.Dataset` with
variables providing the trace gas columns and their uncertainties (e.g. "CO2"
and "CO2_precision") that need to have a `units` attribute for automatic unit
conversion and a `noise_level` attribute that is used as random uncertainty.
In addition, the central longitude and latitude of the pixels need to be
provided as `lon` and `lat`.

Sentinel-5P/TROPOMI
^^^^^^^^^^^^^^^^^^^
Sentinel-5P/TROPOMI images can read using `xr.open_dataset` after being downloaded
and prepared by the `ddeq.download_S5P` module.

To iterate over TROPOMI data, a dataset class can be used, which can be used to
load TROPOMI data on demand. The class is used by the divergence method.

.. autoclass:: ddeq.sats.Level2TropomiDataset
   :special-members: __init__
   :members: read_date


Synthetic CO2M images from the SMARTCARB dataset
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autofunction:: ddeq.smartcarb.read_level2

.. autoclass:: ddeq.smartcarb.Level2Dataset
   :special-members: __init__
   :members: read_date


Synthetic CO2M images from the CoCO2 library of plumes
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. autofunction:: ddeq.coco2.read_level2



Wind fields
-----------

.. autofunction:: ddeq.wind.read_at_sources

.. autofunction:: ddeq.wind.read_field