# Data-driven emission quantification

ddeq is Python library for data-driven emission quantification of emission hot
spots such as cities, power plants and industrial facilities.


## Documentation

The full documentation is hosted at https://ddeq.readthedocs.io.