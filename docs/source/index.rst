.. ddeq Python library documentation master file, created by
   sphinx-quickstart on Mon Nov  6 15:24:22 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to ddeq Python library's documentation!
===============================================

*ddeq* is Python library for data-driven emission quantification of emission
hot spots such as cities, power plants and industrial facilities.


Contents
--------

.. toctree::
   :maxdepth: 2

   installation
   usage
   tutorial
   biblio

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
