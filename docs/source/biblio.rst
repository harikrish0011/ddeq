Bibliography
============

Reference publication
---------------------
*   G. Kuhlmann, E. Koene, S. Meier, D. Santaren, G. Broquet, F. Chevallier, J. Hakkarainen, J. Nurmela, L. Amarós, J. Tamminen and D Brunner: The ddeq Python library for data-driven emission quantification of hot spots (Version 1.0) in preparation.


Publications that use the ddeq Python library
---------------------------------------------

*   J. Dumont Le Brazidec, P. Vanderbecken, A. Farchi, G. Kuhlmann, G. Broquet, and M. Bocquet: Deep learning ap-plied to power plant emissions quantification using XCO2 synthetic satellite images, Geo. Model. Dev., in review.
*   J. Dumont Le Brazidec, P. Vanderbecken, A. Farchi, M. Bocquet, J. Lian, G. Broquet, G. Kuhlmann, A. Danjou and T. Lauvaux: Segmentation of XCO2 images with deep learning: application to synthetic plumes from cities and power plants, Geo. Model. Dev., doi: 10.5194/gmd-16-3997-2023, 2023.
*   J. Hakkarainen, I. Ialongo, E. Koene, M. E. Szelag, J. Tamminen, G. Kuhlmann and D. Brunner: Analyzing local car-bon dioxide and nitrogen oxide emissions from space using the divergence method: An application to the synthetic SMARTCARB dataset, Front. Remote Sens., doi: 10.3389/frsen.2022.878731, 2022.
*   G. Kuhlmann , K. L. Chan, S. Donner, Y. Zhu, M. Schwaerzel, S. Dörner, J. Chen, A. Hueni, D. H. Nguyen, A. Damm, A. Schütt, F. Dietrich, D. Brunner, C. Liu, B. Buchmann, T. Wagner and M. Wenig: Mapping the spatial distribution of NO2 with in situ and remote sensing instruments during the Munich NO2 imaging campaign, Atmos. Meas. Tech., doi: 10.5194/amt-15-1609-2022, 2022.
*   G. Kuhlmann, S. Henne, Y. Meijer and D. Brunner: Quantifying CO2 emissions of power plants with CO2 and NO2 imaging satellites, Front. Remote Sens., doi: 10.3389/frsen.2021.689838, 2021.
*   G. Kuhlmann, D. Brunner, G. Broquet, and Y. Meijer: Quantifying CO2 emissions of a city with the Copernicus An-thropogenic CO2 Monitoring satellite mission, Atmos. Meas. Tech., doi: 10.5194/amt-13-6733-2020, 2020.
*   G. Kuhlmann, G. Broquet, J. Marshall, V. Clément, A. Löscher, Y. Meijer, and D. Brunner: Detectability of CO2 emis-sion plumes of cities and power plants with the Copernicus Anthropogenic CO2 Monitoring (CO2M) mission, Atmos. Meas. Tech., doi: 10.5194/amt-12-6695-2019, 2019.