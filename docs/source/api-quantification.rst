Emission quantification
=======================
To quantify the emissions, five modules are provided implementing a Gaussian
plume inversion, two cross-sectional flux methods, integrated mass enhancement
and divergence method.

Gaussian plume (GP) inversion
-----------------------------

.. autofunction:: ddeq.gauss.estimate_emissions


Cross sectional flux (CSF)
--------------------------

.. autofunction:: ddeq.csf.estimate_emissions


Light cross sectional flux (LCSF)
---------------------------------

.. autofunction:: ddeq.lcsf.estimate_emissions


Integrated mass enhancement (IME)
---------------------------------

.. autofunction:: ddeq.ime.estimate_emissions


Diverence method (DIV)
----------------------

.. autofunction:: ddeq.div.estimate_emissions