Pre-processing
==============
The second component provides pre-processing of the data, which includes plume
detection, conversion between coordinate systems, unit conversions, and
estimation of the background field.

Plume detection algorithms
--------------------------
The plume detection uses the function `ddeq.dplume.detect_plumes` for detecting
plumes in the remote sensing image and
`ddeq.plume_coords.compute_plume_line_and_coords` for computing the center curve
and computing along- and across-plume coordinates.

.. autofunction:: ddeq.dplume.detect_plumes

.. autofunction:: ddeq.plume_coords.compute_plume_line_and_coords

Prepare data
------------
The data can be prepared for emission quantification with
`ddeq.emissions.prepare_data` function, which estimates the background field
(using `ddeq.background.estimate`), computes the local enhancement above the
background (plume signal) and converts units to mass column densities
(in kg m-2).

.. autofunction:: ddeq.emissions.prepare_data

.. autofunction:: ddeq.background.estimate